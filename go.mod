module gitlab.wikimedia.org/frankie/aqsassist

go 1.17

require (
	github.com/stretchr/testify v1.8.1
	github.com/valyala/fasthttp v1.44.0
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.7.2
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
