package aqsassist

import (
	"github.com/valyala/fasthttp"
)

// NotFoundHandler is the HTTP handler when no match routes are found.
type NotFoundHandler struct {
}

func (s *NotFoundHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	BadRequestValidationWrapper(ValidationWrapper{
		Ctx:    ctx,
		Detail: "Invalid route",
		URL:    nil,
	})
}
