package aqsassist

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/valyala/fasthttp"
	"schneider.vip/problem"
)

var HeadersToAdd = map[string]string{
	"access-control-allow-headers":  "accept, content-type, content-length, cache-control, accept-language, api-user-agent, if-match, if-modified-since, if-none-match, dnt, accept-encoding",
	"access-control-allow-methods":  "GET,HEAD",
	"access-control-allow-origin":   "*",
	"access-control-expose-headers": "etag",
	"cache-control":                 "s-maxage=14400, max-age=14400",
	"referrer-policy":               "origin-when-cross-origin",
	"x-content-type-options":        "nosniff",
	"x-xss-protection":              "1; mode=block",
	"x-frame-options":               "deny",
	"content-type":                  "application/json; charset=utf-8",
	"content-security-policy":       "default-src 'none'; frame-ancestors 'none'",
}

func ValidateTimestamp(param string) (string, error) {
	var err error
	var timestamp string

	// We accept timestamp parameters of two forms, YYYYMMDD and YYYYMMDDHH.
	// If timestamp parameter is 8 bytes length (8 ASCII runes),
	// then suffix the string with "00".

	if len(param) == 8 {
		timestamp = fmt.Sprintf("%s00", param)
	} else {
		timestamp = param
	}

	if _, err = time.Parse("2006010203", timestamp); err != nil {
		return "", err
	}

	return timestamp, nil
}

func TrimProjectDomain(param string) string {
	return strings.TrimPrefix(strings.TrimSuffix(strings.ToLower(param), ".org"), "www.")
}

func SetSecurityHeaders(w http.ResponseWriter) {
	for headerName, headerValue := range HeadersToAdd {
		w.Header().Set(headerName, headerValue)
	}
}

func FilterAgent(agent string, spider int, user int) (int, error) {

	if agent == "user" {
		return user, nil
	} else if agent == "spider" {
		return spider, nil
	} else if agent == "all-agents" {
		return spider + user, nil
	} else {
		return 0, errors.New("invalid agent")
	}
}

func ValidateDuration(start string, end string, granularity string) (string, string, error) {

	timeStart, _ := time.Parse("2006010203", start)
	timeEnd, _ := time.Parse("2006010203", end)

	if granularity == "monthly" {
		if timeEnd.After(timeStart.AddDate(0, 2, 0)) {
			return start, end, nil
		} else if timeEnd.After(timeStart.AddDate(0, 1, 0)) {
			if timeStart.Day() != 01 && timeEnd.Add(time.Hour*24).Day() != 01 {
				return "", "", errors.New("no full months found in specified date range")
			}
			return start, end, nil
		} else {
			if timeStart.Day() != 01 || timeEnd.Add(time.Hour*24).Day() != 01 {
				return "", "", errors.New("no full months found in specified date range")
			}
			return start, end, nil
		}
	} else {
		return start, end, nil
	}
}

func StartBeforeEnd(start string, end string) error {
	timeStart, e := time.Parse("2006010203", start)
	if e != nil {
		return errors.New("start timestamp is invalid, must be a valid date in YYYYMMDD format")
	}
	timeEnd, e := time.Parse("2006010203", end)
	if e != nil {
		return errors.New("start timestamp is invalid, must be a valid date in YYYYMMDD format")
	}

	if timeStart.After(timeEnd) {
		return errors.New("start timestamp should be before the end timestamp")
	} else {
		return nil
	}
}

func CreateProblem(status int, detail string, uri string) *problem.Problem {
	return problem.New(
		problem.Type("about:blank"),
		problem.Title(http.StatusText(status)),
		problem.Custom("method", strings.ToLower(http.MethodGet)),
		problem.Status(status),
		problem.Custom("detail", detail),
		problem.Custom("uri", uri),
	)
}

type ValidationWrapper struct {
	Ctx    *fasthttp.RequestCtx
	Detail string
	URL    *string
}

func BadRequestValidationWrapper(input ValidationWrapper) {
	statusCode := http.StatusBadRequest
	url := string(input.Ctx.Request.URI().RequestURI())

	// 	if url is passed, overwrite the url from ctx
	if input.URL != nil {
		url = *input.URL
	}
	problemResp := CreateProblem(statusCode, input.Detail, url).JSON()
	input.Ctx.SetBody(problemResp)
	input.Ctx.SetStatusCode(statusCode)
}

func InternalServerErrorWrapper(input ValidationWrapper) {
	statusCode := http.StatusInternalServerError
	url := string(input.Ctx.Request.URI().RequestURI())

	// 	if url is passed, overwrite the url from ctx
	if input.URL != nil {
		url = *input.URL
	}
	problemResp := CreateProblem(statusCode, input.Detail, url).JSON()
	input.Ctx.SetBody(problemResp)
	input.Ctx.SetStatusCode(statusCode)
}
