package test

import (
	"context"
	"encoding/json"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"

	"strings"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

func TestValidateTimestamp(t *testing.T) {
	timestamp := "20220101"

	validatedTimestamp, err := aqsassist.ValidateTimestamp(timestamp)

	if len(validatedTimestamp) != 10 || err != nil {
		t.Fatalf(`Got '%s,', want 2022010100`, validatedTimestamp)
	}
}

func TestInvalidTimestamp(t *testing.T) {
	timestamp := ""

	_, err := aqsassist.ValidateTimestamp(timestamp)

	if err == nil {
		t.Fatalf(`Got %s, want parsing error`, err)
	}
}

func TestTrimProjectDomain(t *testing.T) {
	domain := "www.es.wikipedia.org"

	trimmedDomain := aqsassist.TrimProjectDomain(domain)

	if trimmedDomain != "es.wikipedia" {
		t.Fatalf(`Got %s, want es.wikipedia`, trimmedDomain)
	}
}

func TestFilterAgent(t *testing.T) {
	agent := "all-agents"

	total, _ := aqsassist.FilterAgent(agent, 1, 1)

	if total != 2 {
		t.Fatalf(`Got %d, want 2`, total)
	}
}

func TestBadFilterAgent(t *testing.T) {
	agent := "all"

	total, err := aqsassist.FilterAgent(agent, 1, 1)

	if total != 0 || err == nil {
		t.Fatalf(`Got %d, want to throw an error`, total)
	}
}

// ValidateDuration for monthly granularity
func TestFullMonth(t *testing.T) {
	start := "2022010100"
	end := "2022013100"
	granularity := "monthly"

	_, _, err := aqsassist.ValidateDuration(start, end, granularity)

	if err != nil {
		t.Fatalf(`Got %s, want nil`, err)
	}

}

func TestLessThanOneMonth(t *testing.T) {
	//Duration shorter than a month,
	//no full months in range
	start := "2022010100"
	end := "2022010200"
	granularity := "monthly"

	s, e, err := aqsassist.ValidateDuration(start, end, granularity)

	if err == nil {
		t.Fatalf("Got %s, %s, want to throw an err", s, e)
	}
}

func TestLessThanTwoMonths(t *testing.T) {
	//Duration longer than a month,
	//no full months in range
	start := "2022010200"
	end := "2022022700"
	granularity := "monthly"

	s, e, err := aqsassist.ValidateDuration(start, end, granularity)

	if err == nil {
		t.Fatalf("Got %s, %s, want to throw an err", s, e)
	}
}

//ValidateDuration for daily granularity

func TestDaily(t *testing.T) {
	//Duration shorter than a month,
	//no full months in range
	start := "2022010100"
	end := "2022010200"
	granularity := "daily"

	_, _, err := aqsassist.ValidateDuration(start, end, granularity)

	if err != nil {
		t.Fatalf("Got %s, want nil", err)
	}
}

func TestStartBeforeEnd(t *testing.T) {

	start := "2021020100"
	end := "2021010100"

	err := aqsassist.StartBeforeEnd(start, end)

	if err == nil {
		t.Fatalf("Err %s, want error", err)
	}

	err = aqsassist.StartBeforeEnd(start, start)

	if err != nil {
		t.Fatalf("Got %s, want nil", err)
	}
}

func TestCreateProblem(t *testing.T) {

	str := "error message"
	uri := "http://example.com"

	prob := aqsassist.CreateProblem(http.StatusNotFound, str, uri)

	expectedProblem := problem.New(
		problem.Type("about:blank"),
		problem.Title(http.StatusText(http.StatusNotFound)),
		problem.Custom("method", strings.ToLower(http.MethodGet)),
		problem.Status(http.StatusNotFound),
		problem.Custom("detail", str),
		problem.Custom("uri", uri),
	)

	require.Equal(t, prob, expectedProblem)

	_, err := json.Marshal(prob)
	if err != nil {
		t.Fatalf("Could not marshal problem to json: %s", err)
	}
}

// serve serves mock http request using provided fasthttp handler
func serve(handler fasthttp.RequestHandler, req *http.Request) (*http.Response, error) {
	// creates a mock in memory listener.
	ln := fasthttputil.NewInmemoryListener()
	defer ln.Close()

	go func() {
		err := fasthttp.Serve(ln, handler)
		if err != nil {
			panic("Error starting server")
		}
	}()

	client := http.Client{
		Transport: &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return ln.Dial()
			},
		},
	}

	return client.Do(req)
}

func TestBadRequestValidationWrapper(t *testing.T) {
	r, err := http.NewRequest("GET", "http://localhost:8080/", nil)
	if err != nil {
		t.Error(err)
	}

	res, err := serve(func(ctx *fasthttp.RequestCtx) {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Error Making Request",
			URL:    nil,
		})
	}, r)

	if err != nil {
		t.Error(err)
	}

	if res.StatusCode != 400 {
		t.Fatalf("Got %d instead but expected a 400", res.StatusCode)
	}
}

func TestInternalServerErrorWrapper(t *testing.T) {
	r, err := http.NewRequest("GET", "http://localhost:8080/", nil)
	if err != nil {
		t.Error(err)
	}

	res, err := serve(func(ctx *fasthttp.RequestCtx) {
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Error Making Request",
			URL:    nil,
		})
	}, r)

	if err != nil {
		t.Error(err)
	}

	if res.StatusCode != 500 {
		t.Fatalf("Got %d instead but expected a 500", res.StatusCode)
	}
}

func TestNotFoundHandler(t *testing.T) {
	r, err := http.NewRequest("GET", "http://localhost:8080/", nil)
	if err != nil {
		t.Error(err)
	}

	notFoundHandler := &aqsassist.NotFoundHandler{}

	res, err := serve(func(ctx *fasthttp.RequestCtx) {
		notFoundHandler.HandleFastHTTP(ctx)
	}, r)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, 400, res.StatusCode)
}

func TestValidateLogLevel(t *testing.T) {
	err := aqsassist.ValidateLogLevel("debug")

	if err != nil {
		t.Error(err)
	}
}

func TestSecureTestURL(t *testing.T) {
	apiTestUrl := aqsassist.TestURL(aqsassist.TestURLParam{
		Fallback: "http://localhost:8080",
	})

	url := apiTestUrl("test-endpoint")

	if url != "http://localhost:8080/test-endpoint" {
		t.Error("Urls are not the same")
	}
}

func TestSetSecurityHeaders(t *testing.T) {
	rec := httptest.NewRecorder()

	aqsassist.SetSecurityHeaders(rec)
	for headerName, headerValue := range aqsassist.HeadersToAdd {
		if rec.Header().Get(headerName) != headerValue {
			t.Error("Header value " + headerName + " incorrect or missing ")

		}
	}
}
